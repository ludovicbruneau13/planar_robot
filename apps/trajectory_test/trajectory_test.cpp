#include "trajectory_generation.h"
#include <iostream>

using namespace std;

//const double piIn = 1;
//const double pfIn = 2;

const double DtIn = 10.00;
double t    = 0.00;
double dt   = 0.01;

const Eigen::Vector2d Xi (0, 0);
const Eigen::Vector2d Xf (1, 1);

Eigen::Vector2d PosXt (0, 0);
Eigen::Vector2d PosXdt (0, 0);

Eigen::Vector2d VelaXt (0, 0);
Eigen::Vector2d VelnXt (0, 0);

//Polynomial MyPolynome(piIn, pfIn, DtIn);
Point2Point MyPosition(Xi, Xf, DtIn);


int main(){

    //MyPolynome.update(piIn, pfIn, DtIn);
    //MyPolynome.p(t);
    //MyPolynome.dp(t);

    for ( t = 0; t < DtIn; t++)
    {
        cout << "\n t = " << t << "\n";

        PosXt  = MyPosition.X(t);
        PosXdt = MyPosition.X(t+dt);

        VelaXt = MyPosition.dX(t);
        VelnXt(0) = (PosXdt(0) - PosXt(0) )/ dt;
        VelnXt(1) = (PosXdt(1) - PosXt(1) )/ dt;

        cout << "--Vitesse Numérique :\n" << VelnXt << "\n--Vitesse Analytique :\n" << VelaXt << "\n";   
    }
}