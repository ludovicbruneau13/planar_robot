#include "kinematic_model.h"
#include <iostream>
#include <cmath>

using namespace std;

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI_4
  // For a small variation of q, compute the variation on X and check dx = J . dq 
  const double pi = 3.1415; 
  const double l1 = 0.10; //= [m]
  const double l2 = 0.10; //= [m]


  Eigen::Vector2d X (0,0);
  Eigen::Matrix2d J = Eigen::Matrix2d::Zero();
  const Eigen::Vector2d Q (pi/3, 0);

  Eigen::Vector2d X_2 (0,0);
  Eigen::Matrix2d J_2 = Eigen::Matrix2d::Zero();
  const Eigen::Vector2d Q_2 = Q*1.1;

  RobotModel MyRobot(l1, l2);
  MyRobot.FwdKin(X, J, Q);
  MyRobot.FwdKin(X_2, J_2, Q_2);

  Eigen::Vector2d Variation_X = X_2 - X;
  Eigen::Vector2d Variation_Q = Q_2 - Q;
  Eigen::Vector2d Variation_X_compare = J * Variation_Q;

  cout << "\n\n Q = \n" << Q << "\n";
  cout << "\n\n Q_2 = \n" << Q_2 << "\n";
  cout << "\n\n Variation de X dx= X1 - X2 \n" << Variation_X << "\n";
  cout << "\n\n Variation de X dx= J * dq \n" << Variation_X_compare << "\n";
}
