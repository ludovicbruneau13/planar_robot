#include "control.h"
#include "trajectory_generation.h"
#include <iostream>
#include <cmath>

using namespace std;


int main(){
  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position 
  //      q   = M_PI_2, M_PI_4
  // and  
  //      l1  = 0.4, l2 = 0.5
  // simulate a motion achieving 
  //      Xf  = 0.0, 0.6

const double l1 = 0.4; 
const double l2 = 0.5; 
double t = 0;
const double dt = 0.01;
const double tf = 10.0;

Eigen::Vector2d x ;             //def Current pos
Eigen::Vector2d xd;             //def Desired pos
Eigen::Vector2d xi;             //def Initial pos
const Eigen::Vector2d xf (0.0 , 0.6);  //def Final pos
Eigen::Matrix2d J;                    //Jacobian initialisation 
Eigen::Vector2d q (M_PI_2, M_PI_4);   //Joint initialisation
Eigen::Vector2d   Dqd;                //def Joint Diff Velocity
Eigen::Vector2d Dxd_ff;               //def Desired Diff Velocity

RobotModel MyRobot(l1, l2);           //RobotModel Init
Controller MyController( MyRobot);    //Controler Init
MyRobot.FwdKin(xi, J, q);             //def Current Position
Point2Point MyPosition(xi, xf, tf);   //Trajectory Init

cout <<"t, xd_x, x_x, xd_y, x_y, q_1, q_2" << "\n"; //Plot's variables
  while (t <= tf)                                   //Control loop
  {
    xd = MyPosition.X(t);                  //desired position
    Dxd_ff = MyPosition.dX(t);             //velocity diff desired
    Dqd = MyController.Dqd(q, xd, Dxd_ff); //compute joint diff velocity
    q = q + Dqd*dt;                        // compute joint position Q + Qvel*dt
    t = t + dt;                            //time increment t++
    MyRobot.FwdKin(x, J, q);               //Currunt Position X [x, y]
    cout << t << "," << xd(0) << "," << x(0) << "," << xd(1) <<  "," << x(1) << "," <<  q(0) << "," << q(1) << endl; //print variables
  }
}
//=== END ===