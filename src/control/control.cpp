#include "control.h"
#include <iostream>

using namespace std;

Controller::Controller(RobotModel &rmIn):
  model  (rmIn)
{
}

Eigen::Vector2d Controller::Dqd ( 
          const Eigen::Vector2d & q,
          const Eigen::Vector2d & xd,
          const Eigen::Vector2d & Dxd_ff
                      )
{  
  //TODO Compute joint velocities able to track the desired cartesian position
  model.FwdKin(X, J, q); //def current position
  dX_desired = (xd - X)*kp + Dxd_ff;  //differential velocity desired
  return J.inverse() * (dX_desired + kp*(xd-X)); //Invert kinetic Model dot_Q
}

