#include "trajectory_generation.h"
#include <iostream>

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double &DtIn)
{
  // TODO initialize the object polynomial coefficients
  pi = piIn;                 // def initial pos
  pf = pfIn;                 // def final pos
  Dt = DtIn;                 // def final time
  a[0] = pi;                 // def a0
  a[1] = 0;                  // def a1
  a[2] = 0;                  // def a2
  a[3] = 10 * pf - 10 * pi;  // def a3
  a[4] = -15 * pf + 15 * pi; // def a4
  a[5] = 6 * pf - 6 * pi;    // def a5
};

void Polynomial::update(const double &piIn, const double &pfIn, const double &DtIn)
{
  // TODO update polynomial coefficients
  pf = pfIn; // def final pos
  pi = piIn; // def initial pos
  Dt = DtIn; // def final time

  a[0] = pi;                 // def a0
  a[1] = 0;                  // def a1
  a[2] = 0;                  // def a2
  a[3] = 10 * pf - 10 * pi;  // def a3
  a[4] = -15 * pf + 15 * pi; // def a4
  a[5] = 6 * pf - 6 * pi;    // def a5
};

const double Polynomial::p(const double &t)
{
  // TODO compute position
  const double p = a[0] + a[1] * pow(t / Dt, 1) + a[2] * pow(t / Dt, 2) + a[3] * pow(t / Dt, 3) + a[4] * pow(t / Dt, 4) + a[5] * pow(t / Dt, 5); // def polynome t/dt -> t=dt xd = xf
  return p;
};

const double Polynomial::dp(const double &t)
{
  // TODO compute velocity
  const double dp = 3 * a[3] * pow(t / Dt, 2) / Dt + 4 * a[4] * pow(t / Dt, 3) / Dt + 5 * a[5] * pow(t / Dt, 4) / Dt; // def deriv of pol -> velocity
  return dp;
};

Point2Point::Point2Point(const Eigen::Vector2d &xi, const Eigen::Vector2d &xf, const double &DtIn)
{
  // TODO initialize object and polynomials
  polx.update(xi(0), xf(0), DtIn); // define init pos x, final pos x, final time
  poly.update(xi(1), xf(1), DtIn); // define init pos y, final pos y, final time
}

Eigen::Vector2d Point2Point::X(const double &time)
{
  // TODO compute cartesian position
  Eigen::Vector2d XPos(polx.p(time), poly.p(time)); // define Xd(t)
  return XPos;
}

Eigen::Vector2d Point2Point::dX(const double &time)
{
  // TODO compute cartesian velocity
  Eigen::Vector2d XVel(polx.dp(time), poly.dp(time)); // define dot_Xd(t)
  return XVel;
}