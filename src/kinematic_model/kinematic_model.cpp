#include "kinematic_model.h"
#include "cmath"
#include <iostream>

using namespace std;

RobotModel::RobotModel(const double &l1In, const double &l2In) : l1(l1In), l2(l2In){};

void RobotModel::FwdKin(Eigen::Vector2d &xOut, Eigen::Matrix2d &JOut, const Eigen::Vector2d &qIn)
{
  // TODO Implement the forward and differential kinematics
  Eigen::Matrix3d J = Eigen::Matrix3d::Zero();
  double q1 = qIn(0);                         // define joint 1
  double q2 = qIn(1);                         // define joint 2
  xOut[0] = l2 * cos(q1 + q2) + l1 * cos(q1); // define x pos X(q1, q2)
  xOut[1] = l2 * sin(q1 + q2) + l1 * sin(q1); // define y pos Y(q1, q2)
  // define Jacobian Matrix
  JOut(0, 0) = -l2 * sin(q1 + q2) - l1 * sin(q1); // dx/dq1
  JOut(0, 1) = -l2 * sin(q1 + q2);                // dx/dq2
  JOut(1, 0) = l2 * cos(q1 + q2) + l1 * cos(q1);  // dy/dq1
  JOut(1, 1) = l2 * cos(q1 + q2);                 // dy/dq2
}